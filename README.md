# www.ing.es to homebank conversion tool

This tool convert xls movements exported from www.ing.es to Homebank csv compatible import file.  
Gets xls with the movements formated in spanish language and converts it to a valid csv to be imported in Homebank software.  
Homebank is a free personal finance software for money management.  
www.ing.es is the spanish portal of the banking and financial services corporation.  

Install tkinter
```
sudo apt-get install python3-tk
```
Install pandas
```
pip3 install pandas
```
Install xlrd
```
pip3 install xlrd
``` 
