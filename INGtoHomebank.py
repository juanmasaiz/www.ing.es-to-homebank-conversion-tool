import tkinter as tk
from tkinter import filedialog
from tkinter import messagebox
import pandas as pd

root= tk.Tk()

label1 = tk.Label(root, text='ING/Homebank Conversion Tool', bg = 'lightsteelblue2')
label1.config(font=('helvetica', 20))
label1.grid(column=0, row=0)

def getExcel ():
    global data
    
    import_file_path = filedialog.askopenfilename()
    data = pd.read_excel (import_file_path, skiprows=list(range(4)))
    #print('print del excel')
    #print(data)
    
browseButton_Excel = tk.Button(text="Import www.ing.es xls exported file", command=getExcel, bg='green', fg='white', font=('helvetica', 12, 'bold'))
browseButton_Excel.grid(column=0, row=1)

def convertToCSV ():
    global data
    export_file_path = filedialog.asksaveasfilename(defaultextension='.csv')
    #print('Primer print')
    #print(data)
    #data = data.iloc[4:]
    #print('Segundo print')
    #print(data)
    data = data.replace({'DESCRIPCIÓN': {';':'',',':''}}) #Changing semicolon to comma
    data = data.drop(columns=['CATEGORÍA', 'SUBCATEGORÍA', 'COMENTARIO']) #Deleting unnecessary columns
    data.rename(columns={'F. VALOR':'date','DESCRIPCIÓN':'info','IMPORTE (€)':'amount'}, inplace=True) #Column rename
    data.insert(1,'paymode','0') #Insert new columns
    data.insert(3,'payee','')
    data.insert(4,'memo','')
    data.insert(6,'category','')
    data.insert(7,'tags','')
    data['memo'] = data['info'] #Copy info to memo column
    data.to_csv (export_file_path,date_format='%d-%m-%Y', index = None, header=True, sep=';')
    #print('Último print')
    print(data) #Show result 

saveAsButton_CSV = tk.Button(text='Convert Excel to Homebank compatible CSV', command=convertToCSV, bg='green', fg='white', font=('helvetica', 12, 'bold'))
saveAsButton_CSV.grid(column=0, row=2)

def exitApplication():
    MsgBox = tk.messagebox.askquestion ('Exit Application','Are you sure you want to exit the application',icon = 'warning')
    if MsgBox == 'yes':
       root.destroy()
     
exitButton = tk.Button (root, text='Exit Application',command=exitApplication, bg='brown', fg='white', font=('helvetica', 12, 'bold'))
exitButton.grid(column=0, row=3)

root.mainloop()